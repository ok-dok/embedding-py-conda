#!/usr/bin/env bash

# m4_ignore(
echo "This is a script template, not the script (yet) - pass it to 'argbash' to convert to runnable script." >&2
exit 11  #)Created by argbash-init v2.10.0
# ARG_OPTIONAL_BOOLEAN([yes], [y], "Do not ask for confirmation.")
# ARG_OPTIONAL_BOOLEAN([force], [f], "Force to install or update(full) when already exists")
# ARG_OPTIONAL_BOOLEAN([update], [u], "Update current version to latest")
# ARG_OPTIONAL_BOOLEAN([install-conda], [i], "Install miniconda ignored existing python environment")
# ARG_OPTIONAL_BOOLEAN([offline], [],"Offline mode. Don't connect to the Internet")
# ARGBASH_SET_DELIM([ ])
# ARG_OPTION_STACKING([none])
# ARG_RESTRICT_VALUES([none])
# ARG_HELP([<A tool to check if python environment exists or install it by miniconda >])
# ARGBASH_GO

# [ <-- needed because of Argbash

# vvv  PLACE YOUR CODE HERE  vvv

error(){
  echo -e "\e[31m[ERRO]\e[0m $*" #]]
}
info(){
  echo -e "\e[32m[INFO]\e[0m $*" #]]
}
warn() {
  echo -e "\e[33m[WARN]\e[0m $*" #]]
}
highlight() {
    echo -e "\e[36m$*\e[0m" #]]
}

input_confirm() {
    while [ "$confirm" == "" ]; do
      read -p "$@" confirm
    done
    temp=$(echo "$confirm" | tr [a-z] [A-Z])
    if [ "$temp" != "YES" ] && [ "$temp" != "Y" ]; then
        return 1
    else
        return 0
    fi
}

OS_TYPE=
OS_ARCH=

DEFAULT_INSTALL_PATH="$HOME/miniconda3"
#
#install_by_homebrew() {
#    brew install python3 && return 0 || return 1
#}
#
#install_by_installer_on_mac() {
#    macos_version=$(echo $(sw_vers | grep "ProductVersion" | awk -F':' '{print $2}'))
#    info "your system version info: MacOS $macos_version"
#    # 查找最新的，最匹配当前系统版本的python包
#    latest_py_version=$(curl -sL  https://www.python.org/ftp/python/ | grep -ioE '^<a href="[0-9.]+/">' | grep -ioE '[0-9.]+' | sort -r | head -n 1)
#    matched_py_list=$(curl -sL https://www.python.org/ftp/python/${latest_py_version} | grep -v '.asc'| grep -ioE '^<a href="python-[0-9.]+[-]?.*macos.*">')
#    big_versions=$(echo "$matched_py_list" | grep -ioE 'macos[x]?([0-9]\.?)+' | grep -ioE '([0-9]\.?)+' | sort -r)
#    big_macos_v=$(echo $macos_version | grep -ioE '^[0-9]+')
#    for py_v in "${big_versions[@]}"; do
#      if [ "$(echo $py_v | grep -ioE '^[0-9]+')" -le $big_macos_v ]; then
#        best_py_version=$(echo "$matched_py_list" | grep -ioE 'python-'${latest_py_version}'.*'$py_v'.*pkg')
#        download_url="https://www.python.org/ftp/python/${latest_py_version}/${best_py_version}"
#        break
#      fi
#    done
#    # 如果未发现最合适的版本，则提示并退出
#    [ "$download_url" == "" ] && error "there is no matched python version for your system version" && return 1
#    # 找到合适的版本，提示是否进行安装
#    info "the best latest python version matched: $(highlight ${download_url})"
#    input_confirm "do you want to install it?[Yes/No]"
#    if [ $? == 0 ]; then
#      info "downloading package..."
#      wget "$download_url" -O "$HOME/Downloads/$best_py_version"
#      info "installing package..."
#      sudo installer -pkg "$HOME/Downloads/$best_py_version" -target /
#      rm "$HOME/Downloads/$best_py_version"
#      if [ $(echo $latest_py_version | awk -F. '{print $1}') == 3 ]; then
#        alias python3='/usr/local/bin/python3'
#        alias python='python3'
#      else
#        alias python2='/usr/local/bin/python'
#        alias python='python2'
#      fi
#      info "python installed"
#      return 0
#    else
#      info "install canceled"
#      info "you can download and install the package from \"https://www.python.org/ftp/python/${latest_py_version}\" by manual"
#      return 1
#    fi
#}
#
#install_on_linux(){
#  distributor=$(echo $(lsb_release -i | awk -F: '{print $2}'))
#  if [ $? != 0 ] || [ "$distributor" == "" ]; then
#    distributor=$(cat /etc/*release | grep ^NAME= | tr -d 'NAME="')
#  fi
#  info "your system version: $(uname -srp)"
#  latest_py_version=$(curl -sL  https://www.python.org/ftp/python/ | grep -ioE '^<a href="[0-9.]+/">' | grep -ioE '[0-9.]+' | sort -r | head -n 1)
#  info "the latest python version is ${latest_py_version}"
#  error "linux environment have not support install yet"
#  error "you can install by package manager or compile and install from source code"
#  info "the latest version download url: \"https://www.python.org/ftp/python/${latest_py_version}\""
#  return 1
#}
#
install_on_windows() {
  local pkgexe=""
  if [ "$_arg_offline" = "on" ]; then
    pkgexe=$(ls miniconda | grep -ioE "Miniconda3-latest-${OS_TYPE}-${OS_ARCH}.*\.exe")
  else
    pkgexe=$(curl -k -ssL https://repo.anaconda.com/miniconda/ | grep -ioE '<a href="Miniconda3-latest.*">' | grep -ioE "Miniconda3-latest-${OS_TYPE}-${OS_ARCH}.*\.exe")
    [ $? != 0 ] && error "can not access the internet, please check your network status and make sure you can access the internet and try again, or you can use '--offline' mode that won't use internet."
  fi
  if [ "$pkgexe" = "" ]; then
    error "can not find a suitable version of miniconda. you can visit web site \"https://repo.anaconda.com/miniconda/\" to download the suitable miniconda version and install it by manual" && return 1
  fi
  if [ ! -e "miniconda/$pkgexe" ]; then
    info "downloading conda executable file: https://repo.anaconda.com/miniconda/$pkgexe"
    mkdir -p miniconda && curl -k -L "https://repo.anaconda.com/miniconda/$pkgexe" -o "miniconda/$pkgexe"
  else
    info "using cached package: miniconda/$pkgexe"
  fi
  info "installing conda"
  if [ "$_arg_yes" = "on" ]; then
    cmd //C "start /wait miniconda/$pkgexe /InstallationType=JustMe /AddToPath=1 /RegisterPython=1 /S /D=${DEFAULT_INSTALL_PATH}"
  else
    "miniconda/$pkgexe"
  fi
  return $?
}

install_on_linux_or_macos(){
  local pkgsh=$(curl -k -ssL https://repo.anaconda.com/miniconda/ | grep -ioE '<a href="Miniconda3-latest.*">' | grep -ioE "Miniconda3-latest-${OS_TYPE}-${OS_ARCH}.*\.sh")
  if [ ! -e "miniconda/$pkgsh" ]; then
    info "downloading conda executable file: https://repo.anaconda.com/miniconda/$pkgsh"
    mkdir -p miniconda && curl -k -L "https://repo.anaconda.com/miniconda/$pkgsh" -o "miniconda/$pkgsh"
  else
    info "using cached package: miniconda/$pkgsh"
  fi
  info "installing conda"
  _run_args=""
  if [ "$_arg_force" = "on" ]; then
    _run_args="$_run_args -f"
  fi
  if [ "$_arg_yes" = "on" ]; then
    _run_args="$_run_args -b -p ${DEFAULT_INSTALL_PATH}"
  fi
  info "running install script: bash miniconda/$pkgsh $_run_args"
  bash "miniconda/$pkgsh" $_run_args
  return $?
}

get_sys_info() {
  # 使用uname命令来获取系统信息
  # Parse the OS name and architecture from `uname`.
  local os_type="$(uname | sed 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/')"
  # When running inside Git bash on Windows, `uname` reports "MINGW64_NT".
  case $os_type in mingw64_nt* | msys_nt*)
    os_type="windows"
    ;;
  esac
  # then judge $os_type, such as 'darwin', 'win', 'linux'
  case "$os_type" in
    darwin*)
      os_type="macosx"
      # detect cpu arch info
      arch=$(uname -a | awk -F " " '{print $(NF-1)}' | grep  -ioE '(arm(32|64))|(aarch(32|64))|(x86(([_-]{1}(32|64))|(.{0})))|(amd(32|64))|(i386)' | tr [A-Z] [a-z])
      ;;
    linux*)
      # detect cpu arch info
      arch=$(uname -a | awk -F " " '{print $(NF-1)}' | grep  -ioE '(arm(32|64))|(aarch(32|64))|(x86(([_-]{1}(32|64))|(.{0})))|(amd(32|64))|(i386)' | tr [A-Z] [a-z])
      case $arch in
        arm64|armv8*)
          arch="aarch64"
          ;;
        armv6*|armv7*)
          arch="aarch32"
          ;;
      esac
      ;;
    win*)
      arch=$(wmic cpu get AddressWidth | grep -v "AddressWidth" | xargs echo )
      arch="x86_$arch"
      ;;
    *)
      error "unknown: $os_type" && exit 1
      ;;
  esac
  # uniform naming convention
  case $arch in
    x86[_-]32|i386)
      arch="x86"
      ;;
    amd64)
      arch="x86_64"
      ;;
  esac
  OS_TYPE="$os_type"
  OS_ARCH="$arch"
  return 0
}

# macos 获取cpu芯片信息
# sysctl 'machdep.cpu.brand_string'
# apple M1/2 [Pro|Max]：grep -ioE 'Apple M\d{1}.*((Pro)?|(Max)?)'
# intel x86: grep -ioE 'Intel.*Core.*(i[3579])-\d+.*CPU'

# macos 获取cpu架构信息
# uname -a | awk -F " " '{print $(NF-1)}' | grep  -ioE '(arm(32|64))|(x86([_-]{1}(32|64))|.{0}'

# linux获取架构信息
# lscpu | head -n 1 | awk '/[:：]/ {print $2}'
# output maybe: aarch64, x86_64

locate_conda_path(){
    case "$OS_TYPE" in
      macos*|linux*)
        CONDA_PATH=$(dirname $(which conda 2>/dev/null) 2>/dev/null)
        if [ $? != 0 ]; then
          if ls "${DEFAULT_INSTALL_PATH}/bin/" >/dev/null 2>&1; then
            CONDA_PATH="${DEFAULT_INSTALL_PATH}/bin" && return 0
          else
            return 1
          fi
        fi
        ;;
      win*)
        CONDA_PATH=$(dirname $(which conda 2>/dev/null) 2>/dev/null)
        if [ $? != 0 ]; then
          menu_dir=$(ls "$HOME/AppData/Roaming/Microsoft/Windows/Start Menu/Programs" | grep -i "anaconda")
          lnk_name=$(ls "$HOME/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/$menu_dir/" | grep -vi "powershell" | grep -ioE ".*\.lnk")
          lnk_file="$HOME/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/$menu_dir/$lnk_name"
          if [ "$lnk_name" = "" ] || [ ! -e "$lnk_file" ]; then
              warn "cannot find the miniconda startup lnk file in Start Menu" && return 1
          fi
          CONDA_PATH=$("$(pwd)/bin/lnkparse.exe" "$HOME/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/$menu_dir/$lnk_name" | awk -F' ' '{print $3"\\Scripts"}')
          if [ $? != 0 ]; then
              warn "cannot get the installed path of Miniconda or Anaconda" && return 1
          fi
        fi
      ;;
    esac
    return $?
}

install_conda() {
  if [ "$OS_TYPE" = "" ] || [ "$OS_ARCH" = "" ]; then
    error "cannot detected your system version info" && return 1
  fi
  info "detected system info, OS_TYPE: $OS_TYPE, OS_ARCH: $OS_ARCH"
  # get miniconda3 install package
  case "$OS_TYPE" in
    macosx*|linux*)
      install_on_linux_or_macos || return $?
      info "conda init"
      locate_conda_path
      if [ $? != 0 ]; then
          error "cannot get the installed path of miniconda, you can restart your terminal and run \"$0 -i\" to init and test conda" && return 1
      fi
      . "$CONDA_PATH/activate"
      # init profile
      case $(basename "$SHELL") in
        bash*)
          conda init bash && . "$HOME/.bash_profile"
          ;;
        zsh*)
          conda init zsh && . "$HOME/.zsh_profile"
          ;;
        *)
          conda init "$(basename "$SHELL")" && . "$HOME/*profile"
      esac
      return $?
      ;;
    win*)
      install_on_windows || return $?
      info "conda init"
      locate_conda_path
      if [ $? != 0 ]; then
          error "cannot get conda install path, you can restart your terminal and run \"$0 -i\" to init & test conda" && return 1
      fi
      . "${CONDA_PATH}\activate"
      return $?
      ;;
  esac
}

check_and_install_conda() {
  # 非强制情况下检查conda环境
  if [ "$_arg_force" = "off" ]; then
    info "checking conda environment..."
    locate_conda_path
    if [ $? == 0 ]; then
      conda_exec="${CONDA_PATH}/conda"
      info "located conda executable program: $conda_exec\n conda version: `highlight "$(conda --version)"`" && return 0
    else
      info "there is no conda environment"
    fi
  fi
  # 强制安装或者不存在conda时进行安装
  # 是否开启询问模式
  comfirmed=false
  if [ "$_arg_yes" = "off" ]; then
    input_confirm "Do you want install conda program now?[y/n]" && comfirmed=true
  else
    comfirmed=true
  fi
  if $comfirmed; then
    install_conda && info "conda installed, version: $(bash -c 'conda --version')" && return 0 || error "conda install failed" && return 1
  else
    warn "conda installing canceled, you can install it manual or rerun this script" && return 1
  fi
}

check_python_environment() {
  py_exec=$(which python 2>/dev/null || which python3 2>/dev/null || which python2 2>/dev/null)
  if [ $? == 0 ]; then
    ver=$($py_exec --version 2>&1)
    if [ $? != 0 ]; then
      warn "cannot get python version" && return 1
    else
      info "python version: $(echo $ver | awk -F" " '{print $2}')" && return 0
    fi
  fi
  warn "your system environment doesn't have python program yet"
  return 1
}

main() {
  # 是否忽略python环境检查
  ignored_pycheck=false
  if [ "$_arg_install_conda" = "on" ]; then
    ignored_pycheck=true
  fi
  py_not_exist=false
  if ! $ignored_pycheck ; then
    # 是否python环境存在
    info "checking python environment"
    check_python_environment
    if [ $? == 0 ]; then
      info "python already exists, nothing to do and exit"
      return 0
    else
      py_not_exist=true
    fi
  fi
  # 在忽略python环境检查或者检查后python不存在时执行conda检查和安装
  if $ignored_pycheck || $py_not_exist ; then
    conda_checked=false
    check_and_install_conda
    [ $? == 0 ] && conda_checked=true
    if $conda_checked; then
      conda_exec=$(which conda 2>/dev/null)
      if [ $? != 0 ]; then
        error "conda test failed, please reopen your terminal and run '$0'" && return 1
      fi
      if [ "$_arg_offline" = "off" ]; then
        if "$_arg_update" = "on" ]; then
          update_yes=""
          if [ "$_arg_yes" = "on" ]; then
            update_yes="-y"
          fi
          info "updating conda to the latest version"
          conda update $update_yes -n base -c defaults conda
          if [ $? != 0 ]; then
            error "conda update failed, use '--offline' mode please if you don't want to update to the latest version" && return 1
          fi
        fi
      fi
      info "add conda channels: conda config --add channels conda-forge"
      conda config --add channels conda-forge

      env_test_name="venv-test"
      info "conda env create test: $env_test_name"
      create_args=""
      if [ "$_arg_offline" = "on" ]; then
        create_args="$create_args --offline"
      fi
      conda create $create_args -y -k -p "$(pwd)/$env_test_name" python=3
      info "conda env $env_test_name created"
      . "$(dirname $conda_exec)/activate"
      conda activate "$(pwd)/$env_test_name"
      info "conda env $env_test_name activated"
      py_exec=$(which python3 2>/dev/null|| which python 2>/dev/null)
      $py_exec --version
      info "conda env create test successful"
      conda deactivate
      info "removing conda env $env_test_name"
      conda remove -y -p "$(pwd)/$env_test_name" --all
      info "miniconda environment initialized, and you can use command `highlight "'conda create'"` to create a new python environment"
      return 0
    else
      return 1
    fi
  fi
}

get_sys_info

main
# ^^^  TERMINATE YOUR CODE BEFORE THE BOTTOM ARGBASH MARKER  ^^^

# ] <-- needed because of Argbash
